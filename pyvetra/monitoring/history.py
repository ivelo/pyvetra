import numpy as np
import os
import matplotlib.pyplot as plt
import random
import glob
import datetime
import linecache

from utils import nametmp as nt
from utils import asicinfo
from analyse import analyse
from plot import clb
from decoder import header

class History():
    def __init__(
        self
    ):
        '''
        '''
    
    def canv_history(self, label):
        if label == 'equalisation':
            fig, ax = plt.subplots(figsize = [11,4])
            ax.set_xlabel('Date')
            ax.set_xlim(datetime.date(2022,8,15), datetime.date.today())
            ax.axvspan(datetime.date(2022,8,27), datetime.date(2022,9,30), color = 'b', alpha = 0.18, lw = 0)
            ax.text(datetime.date(2022,8,29), -0.8, 'Standard equalisation', fontsize = 11, rotation = 90, color = 'b')
            ax.axvspan(datetime.date(2023,5,15), datetime.date(2023,7,1), color = 'dodgerblue', alpha = 0.18, lw = 0)
            ax.text(datetime.date(2023,5,17), -0.8, 'Studies', fontsize = 11, rotation = 90, color = 'dodgerblue')
            ax.axvspan(datetime.date(2023,7,1), datetime.date(2023,7,30), color = 'm', alpha = 0.18, lw = 0)
            ax.text(datetime.date(2023,7,3), -0.8, 'Developing new panel', fontsize = 11, rotation = 90, color = 'm')
            ax.axvspan(datetime.date(2023,8,18), datetime.date(2023,10,1), color = 'red', alpha = 0.18, lw = 0)
            ax.text(datetime.date(2023,8,20), -0.8, 'Improved equalisation', fontsize = 11, rotation = 90, color = 'red')
            ax.grid()
            ax.legend()
        return fig, ax

    def database(self, mod):
        c_list = None
        if mod == 25:
            c_list = ['/calib/velo/equalisation/equalis_NewPanel/Summer23/Module25/normal/',
                    '/calib/velo/equalisation/equalis_NewPanel/Summer23_backupchiller/Module25/quick/',
                    '/calib/velo/equalisation/equalis_NewPanel/Summer23/Module25/quick/',
                    #'/calib/velo/equalisation/equalis_NewPanel/20230822/Module29/normal',
                    #'/calib/velo/equalisation/equalis_NewPanel/20230725/Module29/rate_based',
                    #'/calib/velo/equalisation/equalis_NewPanel/20230724/Module29/rate_based',
                    #'/calib/velo/equalisation/equalis_NewPanel/20230721/Module29/rate_based',
                    #'/calib/velo/equalisation/equalis_NewPanel/20230720/Module29/rate_based',
                    #'/calib/velo/equalisation/20230712/Module29_3/rate_based',i
                    '/calib/velo/equalisation/20230707/Module25_DAC_test/rate_based/',
                    '/calib/velo/equalisation/20230706/Module25_quick_testdim_dacthr/rate_based/',
                    '/calib/velo/equalisation/20230704/Module25_dimprint2/rate_based/',
                    '/calib/velo/equalisation/20230703/Module25_QuickScan/rate_based/',
                    '/calib/velo/equalisation/20230701/Module25_IPIXELDAC90/rate_based/',
                    '/calib/velo/equalisation/20230626/Module25_IPIXELDAC95/rate_based/',
                    '/calib/velo/equalisation/20230623/Module25_nohv_test3/rate_based/',
                    '/calib/velo/equalisation/20230621/Module25_IPIXELDAC100/rate_based/',
                    '/calib/velo/equalisation/20230619/Module25/rate_based/',
                    '/calib/velo/equalisation/20230616/Module25/rate_based/',
                    '/calib/velo/equalisation/20230612/Module25_try_full2/rate_based/',
                    '/calib/velo/equalisation/20230608/Module25/rate_based/',
                    '/calib/velo/equalisation/20230607/Module25/rate_based/',
                    '/calib/velo/equalisation/20230601/Module25/rate_based/',
                    '/calib/velo/equalisation/20230601/Module25/rate_based/',
                    '/calib/velo/equalisation/20230531/Module25_newVdac/rate_based/', 
                    '/calib/velo/equalisation/20230531/Module25_1/rate_based/',
                    '/calib/velo/equalisation/20230529/Module25/rate_based/',
                    '/calib/velo/equalisation/20230525/Module25/rate_based/',
                    '/calib/velo/equalisation/20230523/Module25/rate_based/',
                    '/calib/velo/equalisation/08052023/Module25/rate_based/',
                    '/calib/velo/equalisation/12042023/Module25/rate_based/',
                    '/calib/velo/equalisation/28092022/Module25/rate_based/',
                    '/calib/velo/equalisation/22092022/Module25/rate_based/',
                    '/calib/velo/equalisation/17092022/Module25/rate_based/',
                    '/calib/velo/equalisation/06092022/Module25_12092022/rate_based/'
                    ]

        if mod == 29:
            c_list = ['/calib/velo/equalisation/equalis_NewPanel/Summer23/Module29/normal/',
                    '/calib/velo/equalisation/equalis_NewPanel/Summer23_backupchiller/Module29/quick/',
                    '/calib/velo/equalisation/equalis_NewPanel/Summer23/Module29/quick/',
                    '/calib/velo/equalisation/equalis_NewPanel/20230822/Module29/normal/',
                    '/calib/velo/equalisation/equalis_NewPanel/20230725/Module29/rate_based/',
                    '/calib/velo/equalisation/equalis_NewPanel/20230724/Module29/rate_based/',
                    '/calib/velo/equalisation/equalis_NewPanel/20230721/Module29/rate_based/',
                    '/calib/velo/equalisation/equalis_NewPanel/20230720/Module29/rate_based/',
                    '/calib/velo/equalisation/20230712/Module29_3/rate_based/',
                    '/calib/velo/equalisation/20230707/Module29_DAC_test/rate_based/',
                    '/calib/velo/equalisation/20230706/Module29_quick_testdim_dacthr/rate_based/',
                    '/calib/velo/equalisation/20230704/Module29_dimprint2/rate_based/',
                    '/calib/velo/equalisation/20230703/Module29_QuickScan/rate_based/',
                    '/calib/velo/equalisation/20230701/Module29_IPIXELDAC90/rate_based/',
                    '/calib/velo/equalisation/20230626/Module29_IPIXELDAC95/rate_based/',
                    '/calib/velo/equalisation/20230623/Module29_nohv_test3/rate_based/',
                    '/calib/velo/equalisation/20230621/Module29_IPIXELDAC100/rate_based/',
                    '/calib/velo/equalisation/20230619/Module29/rate_based/',
                    '/calib/velo/equalisation/20230616/Module29/rate_based/',
                    '/calib/velo/equalisation/20230612/Module29_try_full2/rate_based/',
                    '/calib/velo/equalisation/20230608/Module29/rate_based/',
                    '/calib/velo/equalisation/20230607/Module29/rate_based/',
                    '/calib/velo/equalisation/20230601/Module29/rate_based/',
                    '/calib/velo/equalisation/20230601/Module29/rate_based/',
                    '/calib/velo/equalisation/20230531/Module29_newVdac/rate_based/', 
                    '/calib/velo/equalisation/20230531/Module29_1/rate_based/',
                    '/calib/velo/equalisation/20230529/Module29/rate_based/',
                    '/calib/velo/equalisation/20230525/Module29/rate_based/',
                    '/calib/velo/equalisation/20230523/Module29/rate_based/',
                    '/calib/velo/equalisation/08052023/Module29/rate_based/',
                    '/calib/velo/equalisation/12042023/Module29/rate_based/',
                    '/calib/velo/equalisation/28092022/Module29/rate_based/',
                    '/calib/velo/equalisation/22092022/Module29/rate_based/',
                    '/calib/velo/equalisation/17092022/Module29/rate_based/',
                    '/calib/velo/equalisation/06092022/Module29_12092022/rate_based/']
        return c_list

    #def find_csv_history(self):
        

    def group_history(self, mod):
        if not os.path.exists('studies'):
            os.makedirs('studies')

        c_list = self.database(mod = mod)
        for tile in asicinfo.tile_list:
            fig, ax = self.canv_history(label = 'equalisation')
            output_path = 'studies/velo_nsplit_Module' + str(mod).zfill(2) + '_' + tile + '.png'
            print('... creating ' + output_path + ' ...')
            n_spread, n_date = [], []
            for entry in c_list:
                print(entry, type(entry))
                data, hdl = self.open_csv(tile + '-0', 'scan0_std', skiprows = 1, return_header = True, path = entry)
                if hdl == None:
                    data, hdl = self.open_csv(tile + '-0', 'Trim0_Noise_Width', skiprows = 1, return_header = True, path = entry)
                if hdl == None:
                    continue

                print('... header ' + hdl)

                mask = self.build_mask_pattern()
                odd_mean = np.mean(data[mask == 1], where = abs(data[mask == 1]) < 100)
                even_mean = np.mean(data[mask == 2], where = abs(data[mask == 2]) < 100)
                #n_spread.append(odd_mean - even_mean)
                n_spread.append(np.mean(data, where = abs(data) < 100))

                hd = header.Header()
                hd.fill(hdl)

                dt = datetime.datetime.fromtimestamp(hd.time)
                print(dt, hd.time)
                n_date.append(dt)

            print(n_date, n_spread)
            ax.plot(n_date, n_spread, '.', color = 'k', linewidth = 0.25, markersize = 9, 
                    linestyle = 'dashed', label = 'Module29_' + tile)
            ax.set_ylabel('Sigma noise [DAC]')
            ax.set_ylim(0,40)
            ax.set_title('Trend for Module29_' + tile)
            fig.savefig(output_path)



