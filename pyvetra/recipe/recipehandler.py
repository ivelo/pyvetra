import numpy as np
import pandas as pd
import os

from utils import asicinfo
from utils import functions as uf

class RecipeHandler():
    def __init__(
        self
    ):
        '''
        '''

    def build_thr_recipe(self, dirc) -> pd.core.frame.DataFrame:
        recipe = uf.build_velo_df(['threshold'])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2)
            for vp in asicinfo.asic_list:
                vp_no = int(vp[2])*3 + int(vp[4])
                if os.path.exists(path + '/control/' + path + '_' + vp + '_threshold_recipe.csv'):
                    thr = np.loadtxt(path + '/control/' + path + '_' + vp + '_threshold_recipe.csv')
                    try:
                        recipe.loc[(recipe['Module'] == path) & (recipe['ASIC'] == vp), 'threshold'] = thr + 10
                        print(recipe.loc[(recipe['Module'] == path) & (recipe['ASIC'] == vp), 'threshold'])
                    except:
                        pass
        recipe.to_csv('trial_recipe_1000e_all.csv')

        return recipe

