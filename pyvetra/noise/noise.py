import numpy as np
import os
import matplotlib.pyplot as plt
import random
import glob
import datetime
import linecache
import logging

from utils import nametmp as nt
from utils import asicinfo, functions
from analyse import analyse
from plot import clb
from decoder import header

class Noise(analyse.Analyse):
    def __init__(
        self
    ):
        """
        """

    def map_universal(self):
        fig, ax = plt.subplots(1,1,figsize = [14, 8])

        ax.xaxis.set_major_locator(plt.MultipleLocator(5))
        ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
        ax.yaxis.set_major_locator(plt.MultipleLocator(1))
        ax.set_xlabel('Module ID', fontsize = 10)
        ax.set_ylabel('ASIC ID', fontsize = 10)
        ax.tick_params(axis = 'both', which = 'major', labelsize = 8)
        ax.set_yticks(ticks = np.linspace(0,11,12), labels = asicinfo.asic_list)
        return fig, ax
    
    #def overview_universal(self):
    #    fig, ax = plt.subplots(1,1,figsize = 14,8)
    #    ax.xaxis.set_major_locator(plt.MultipleLocator(5))
    #    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
    def spread_study(self, atype):
        output_path = 'studies/spread.pdf'
        print('Creating ' + output_path + ' ...')
        fig, ax = self.map_universal()

        n_spread = np.zeros([12,52])
        for mod in range(52):
            path = '22092022/Module' + str(mod).zfill(2) + '/baseline/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        data0 = self.open_csv(id, 'Trim0_Noise_Mean', skiprows = 1, path = path)
                        dataF = self.open_csv(id, 'TrimF_Noise_Mean', skiprows = 1, path = path)
                        #mask = self.build_mask_pattern()
                        #odd_mean = np.mean(data[mask == 1])
                        #even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        #line = linecache.getline('/group/velo/config/VeloPix/Chip/DEFAULT_backup/Module' + str(mod).zfill(2) + 
                        #                    '/Module' + str(mod).zfill(2) + '_' + id + '.dat' , 10)
                        #print(line)
                        #thr = int(line.split('=')[1])
                        #print(thr)
                        mdata0 = np.mean(data0, where = data0 != 0)
                        mdataF = np.mean(dataF, where = dataF != 0)
                        n_spread[id_no, mod] = mdataF - mdata0#thr - np.mean(data, where = data != 0)#(odd_mean - even_mean)
                    except:
                        pass
                    #print(odd_mean - even_mean)

        abstract_plot = ax.imshow(n_spread, interpolation = 'none', origin = 'lower', cmap = 'YlGnBu')
        #ax2.scatter(np.arange(0,624, 1), n_spread.ravel())
        for (j, i), label in np.ndenumerate(n_spread):
            #print(label, type(label))
            
            if np.isnan(label):
                label = 0
            else:
                label = label.astype('int32')
            ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 5)
        #plt.colorbar(cmap = 'cool')
        clba = plt.figure().colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Eff. thr [DAC]", fontsize = 10)
        abstract_plot.set_clim(100,240)  
        fig.savefig(output_path)

        
    def noise_study(self, atype):

        output_path = 'studies/effective_thr.pdf'
        print('Creating ' + output_path + ' ...')
        fig, ax = self.map_universal()

        n_spread = np.zeros([12,52])
        for mod in range(52):
            path = '22092022/Module' + str(mod).zfill(2) + '/baseline/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        data = self.open_csv(id, 'Noise_Predict', skiprows = 0, path = path)
                        #mask = self.build_mask_pattern()
                        #odd_mean = np.mean(data[mask == 1])
                        #even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        line = linecache.getline('/group/velo/config/VeloPix/Chip/DEFAULT_backup/Module' + str(mod).zfill(2) + 
                                            '/Module' + str(mod).zfill(2) + '_' + id + '.dat' , 10)
                        #print(line)
                        thr = int(line.split('=')[1])
                        #print(thr)
                        n_spread[id_no, mod] = thr - np.mean(data, where = data != 0)#(odd_mean - even_mean)
                    except:
                        pass
                    #print(odd_mean - even_mean)

        abstract_plot = ax.imshow(n_spread, interpolation = 'none', origin = 'lower', cmap = 'YlOrRd')
        #ax2.scatter(np.arange(0,624, 1), n_spread.ravel())
        for (j, i), label in np.ndenumerate(n_spread):
            #print(label, type(label))
            
            if np.isnan(label):
                label = 0
            else:
                label = label.astype('int32')
            ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 5)
        #plt.colorbar(cmap = 'cool')
        clba = plt.figure().colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Eff. thr [DAC]", fontsize = 10)
        abstract_plot.set_clim(0,300)  
        fig.savefig(output_path)

    def nsplit_study(self, atype):
        #assuming atype is always 'groups' for now

        if not os.path.exists('studies'):
            os.makedirs('studies')

        output_path = 'studies/velo_nsplit_overiew.png'
        print('Creating ' + output_path + ' ...')
        fig, ax = self.map_universal()

        n_spread = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/' + self.task['scan_type'] + '/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        data = self.open_csv(id, nt.name['n_width_t0'], skiprows = 1, path = path)
                        mask = self.build_mask_pattern()
                        odd_mean = np.mean(data[mask == 1])
                        even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        n_spread[id_no, mod] = (odd_mean - even_mean)
                    except:
                        pass
        abstract_plot = ax.imshow(n_spread, interpolation = 'none', origin = 'lower', cmap = clb.noise_groups())
        #plt.colorbar(cmap = 'cool')
        clba = plt.figure().colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Odd/even col. diff sigma [DAC]", fontsize = 10)
        abstract_plot.set_clim(-6,6)  
        fig.savefig(output_path)
    
    def map_baseline(self, atype):

        if not os.path.exists('studies'):
            os.makedirs('studies')

        output_path = 'studies/velo_baseline_map.png'
        print('Creating ' + output_path + ' ...')
        fig, ax = self.map_universal()

        n_grid = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/' + self.task['scan_type'] + '/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        data = self.open_csv(id, nt.name['n_mean_t0'], skiprows = 1, path = path)
                        id_no = int(id[2])*3 + int(id[4])
                        n_grid[id_no, mod] = np.mean(data, where = data != 0)
                    except:
                        pass
        abstract_plot = ax.imshow(n_grid, interpolation = 'none', origin = 'lower', cmap = clb.noise_groups())
        clba = plt.figure().colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Pixel av. baseline [DAC]", fontsize = 10)
        abstract_plot.set_clim(1200,1900)
        fig.savefig(output_path)

    def temp_list(self):
        calist = ['/calib/velo/equalisation/equalis_NewPanel/Summer23/Module25/normal',
                  '/calib/velo/equalisation/equalis_NewPanel/Summer23_backupchiller/Module25/quick',
                  '/calib/velo/equalisation/equalis_NewPanel/Summer23/Module25/quick',
                  #'/calib/velo/equalisation/equalis_NewPanel/20230822/Module29/normal',
                  #'/calib/velo/equalisation/equalis_NewPanel/20230725/Module29/rate_based',
                  #'/calib/velo/equalisation/equalis_NewPanel/20230724/Module29/rate_based',
                  #'/calib/velo/equalisation/equalis_NewPanel/20230721/Module29/rate_based',
                  #'/calib/velo/equalisation/equalis_NewPanel/20230720/Module29/rate_based',
                  #'/calib/velo/equalisation/20230712/Module29_3/rate_based',i
                  '/calib/velo/equalisation/20230707/Module25_DAC_test/rate_based',
                  '/calib/velo/equalisation/20230706/Module25_quick_testdim_dacthr/rate_based',
                  '/calib/velo/equalisation/20230704/Module25_dimprint2/rate_based',
                  '/calib/velo/equalisation/20230703/Module25_QuickScan/rate_based',
                  '/calib/velo/equalisation/20230701/Module25_IPIXELDAC90/rate_based',
                  '/calib/velo/equalisation/20230626/Module25_IPIXELDAC95/rate_based',
                  '/calib/velo/equalisation/20230623/Module25_nohv_test3/rate_based',
                  '/calib/velo/equalisation/20230621/Module25_IPIXELDAC100/rate_based',
                  '/calib/velo/equalisation/20230619/Module25/rate_based',
                  '/calib/velo/equalisation/20230616/Module25/rate_based',
                  '/calib/velo/equalisation/20230612/Module25_try_full2/rate_based',
                  '/calib/velo/equalisation/20230608/Module25/rate_based',
                  '/calib/velo/equalisation/20230607/Module25/rate_based',
                  '/calib/velo/equalisation/20230601/Module25/rate_based',
                  '/calib/velo/equalisation/20230601/Module25/rate_based',
                  '/calib/velo/equalisation/20230531/Module25_newVdac/rate_based', 
                  '/calib/velo/equalisation/20230531/Module25_1/rate_based',
                  '/calib/velo/equalisation/20230529/Module25/rate_based',
                  '/calib/velo/equalisation/20230525/Module25/rate_based',
                  '/calib/velo/equalisation/20230523/Module25/rate_based',
                  '/calib/velo/equalisation/08052023/Module25/rate_based',
                  '/calib/velo/equalisation/12042023/Module25/rate_based',
                  '/calib/velo/equalisation/28092022/Module25/rate_based',
                  '/calib/velo/equalisation/22092022/Module25/rate_based',
                  '/calib/velo/equalisation/17092022/Module25/rate_based',
                  '/calib/velo/equalisation/06092022/Module25_12092022/rate_based'
                  ]

    def noise_properties_df(self):
        scan_df = functions.build_velo_df(asicinfo.noise_properties)

        index = 0
        for mod in range(52):           
            mod = str(mod).zfill(2)
            for asic in asicinfo.asic_list:
                try:
                    path = f'Module{mod}/normal/'
                    T_0_rate_data = self.open_csv(f'Module{mod}_{asic}', nt.name['n_rate_t0'],
                                                    skiprows=1, path = path)
                    (T_0_rate_mean, T_0_rate_std) = self.mean_std(T_0_rate_data, skip_zeros=True)
                    T_0_base_data = self.open_csv(f'Module{mod}_{asic}', nt.name['n_mean_t0'],
                                                    skiprows=1, path = path)                     
                    (T_0_base_mean, T_0_base_std) = self.mean_std(T_0_base_data, skip_zeros=True)
                    T_0_noise_width_data = self.open_csv(f'Module{mod}_{asic}', nt.name['n_width_t0'],
                                           skiprows = 1, path = path)
                    (T_0_width_mean, T_0_width_std) = self.mean_std(T_0_noise_width_data, skip_zeros=True)
                    mask = self.build_mask_pattern()
                    odd_mean = np.mean(T_0_noise_width_data[mask == 1])
                    even_mean = np.mean(T_0_noise_width_data[mask == 2])
                    distance = odd_mean - even_mean
                                       
                    #filling df
                    array = [ T_0_rate_mean, T_0_rate_std, T_0_base_mean, T_0_base_std, T_0_width_mean, T_0_width_std, distance ]
                    scan_df.loc[index, asicinfo.noise_properties] = array            
                except Exception as et: logging.exception(f'{et}')
                index += 1
        scan_df.to_csv(f'scan_df.csv')

        return scan_df
