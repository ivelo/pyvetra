import pandas as pd
import numpy as np

from utils import asicinfo


# build_velo_df returns a template of df with the list of ASICs
def build_velo_df(col = []) -> pd.core.frame.DataFrame:
    df = pd.DataFrame(np.nan, index = range(624), 
                      columns = ['Module', 'ASIC'] + col)
    #get rid of slice copy warning (not applying here)
    df['Module'][:] = ['Module' + str(int(i/12)).zfill(2) for i in range(624)]
    df['ASIC'][:] = [j for i in range(52) for j in asicinfo.asic_list]
    return df

def velomap2dict(vmap, toint = False):
    if toint:
        return dict(enumerate(map(int, vmap.tolist()), 0))
    else:
        return dict(enumerate(vmap, 0))
    return None