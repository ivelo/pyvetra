asic_list = ['VP0-0', 'VP0-1', 'VP0-2', 'VP1-0', 'VP1-1', 'VP1-2', 
             'VP2-0', 'VP2-1', 'VP2-2', 'VP3-0', 'VP3-1', 'VP3-2' ]

tile_list = ['VP0', 'VP1', 'VP2', 'VP3']

dac_settings = ['IPIXELDAC', 'VFBK', 'VINCAS', 'VPREAMP_CAS', 'VCASDISC', 'IDISC', 'IKRUM', 'IPREAMP'] 

noise_properties = ['T_0_rate_mean', 'T_0_rate_std', 'T_0_base_mean', 'T_0_base_std', 'T_0_noise_width_mean', 'T_0_noise_width_std', 'noise_width_evenodd_dist']
