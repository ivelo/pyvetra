from matplotlib import colors
from matplotlib.colors import ListedColormap, LinearSegmentedColormap
from matplotlib import cm
import numpy as np
import sys

def noise():
  blues = cm.get_cmap('Blues', 256)
  newcolors = blues(np.linspace(0, 1, 256)) 
  #np.flipud() #to reverse colors in a colorbar

  breakpoint = 200 
  newcolors[breakpoint:, 0] = np.linspace(newcolors[breakpoint,0], 1, 256-breakpoint)
  newcolors[breakpoint:, 1] = np.linspace(newcolors[breakpoint,1], 0, 256-breakpoint)
  newcolors[breakpoint:, 2] = np.linspace(newcolors[breakpoint,2], 0, 256-breakpoint)

  newcmap = ListedColormap(newcolors)
  return newcmap

def rate():
  blues = cm.get_cmap('Blues', 256)
  newcolors = blues(np.linspace(0, 1, 256)) 
  #np.flipud() #to reverse colors in a colorbar

  newcmap = ListedColormap(newcolors)
  return newcmap

def trim():
  greys = cm.get_cmap('Greys', 256)
  newcolors = greys(np.linspace(0, 0.55, 256))

  breakpoint = 20 

  newcolors[(256-breakpoint):, 0] = np.linspace(0, 0, breakpoint)
  newcolors[(256-breakpoint):, 1] = np.linspace(0, 0, breakpoint)
  newcolors[(256-breakpoint):, 2] = np.linspace(1, 1, breakpoint)
  
  newcolors[:breakpoint, 0] = np.linspace(1, 1, breakpoint)
  newcolors[:breakpoint, 1] = np.linspace(0, 0, breakpoint)
  newcolors[:breakpoint, 2] = np.linspace(0, 0, breakpoint)

  newcmap = ListedColormap(newcolors)

  return newcmap

def pedestal_clb():

  Greys = cm.get_cmap('viridis', 256)
  newcolors = Greys(np.linspace(0, 1, 256))

  newcmap = ListedColormap(newcolors)

  return newcmap

def mask():

  Greys = cm.get_cmap('Blues', 256)
  newcolors = Greys(np.linspace(0, 1, 256))
  #newcolors = np.flipud(newcolors)

  newcmap = ListedColormap(newcolors)

  return newcmap

def noise_groups():
  blues = cm.get_cmap('PiYG', 256)
  newcolors = blues(np.linspace(0, 1, 256)) 
  #np.flipud() #to reverse colors in a colorbar

  breakpoint = 128
  newcolors[breakpoint:, 0] = np.linspace(1, 0.6, 256-breakpoint)
  newcolors[breakpoint:, 1] = np.linspace(0.7, 0.01, 256-breakpoint)
  newcolors[breakpoint:, 2] = np.linspace(0.7, 0.01, 256-breakpoint)

  newcolors[128, 0] = 1
  newcolors[128, 1] = 1
  newcolors[128, 2] = 1

  newcolors[:breakpoint, 0] = np.linspace(0, 0.7,  256-breakpoint)
  newcolors[:breakpoint, 1] = np.linspace(0, 0.7,  256-breakpoint)
  newcolors[:breakpoint, 2] = np.linspace(0.6, 1,  256-breakpoint)

  newcmap = ListedColormap(newcolors)
  return newcmap

def eq_overview_all():
  greys = cm.get_cmap('Greys', 256)
  newcolors = greys(np.linspace(0, 1, 256))
  
  newcolors[:40, 0] = 1
  newcolors[:40, 1] = 1
  newcolors[:40, 2] = 1

  newcolors[40:125, 0] = 173/255
  newcolors[40:125, 1] = 216/255
  newcolors[40:125, 2] = 230/255

  newcolors[125:210, 0] = 1 #144/255
  newcolors[125:210, 1] = 165/255 #238/255
  newcolors[125:210, 2] = 0 #144/255

  newcolors[210:, 0] = 0 #1
  newcolors[210:, 1] = 1 #165/255
  newcolors[210:, 2] = 0 #

  newcmap = ListedColormap(newcolors)

  return newcmap