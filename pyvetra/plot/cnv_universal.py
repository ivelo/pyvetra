from matplotlib import pyplot as plt
from utils import asicinfo

def velo_map(figsize = [14,8]):
    fig, ax = plt.subplots(figsize = figsize)
    ax.xaxis.set_major_locator(plt.MultipleLocator(5))
    ax.xaxis.set_minor_locator(plt.MultipleLocator(1))
    ax.yaxis.set_major_locator(plt.MultipleLocator(1))
    ax.set_xlabel('Module ID', fontsize = 16)
    ax.set_ylabel('ASIC ID', fontsize = 16)
    ax.tick_params(axis = 'both', which = 'major', labelsize = 15)
    plt.yticks(range(12), asicinfo.asic_list)

    #ax.set_yticks(ticks = range(12), labels = asicinfo.asic_list)
    return fig, ax