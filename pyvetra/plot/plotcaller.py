from matplotlib import pyplot as plt
from plot import plot

class PlotCaller(plot.Plot):
    def __init__(
        self
    ):
        """
        template call functions to plot functions
        canvas definition
        """

    def plot(self, fname, f, id = '', opt = None, path = '') -> None:
        output_path = path + 'plot/' + fname + '.png'
        print('Creating ' + output_path + ' ...')
        fig, ax = plt.subplots(figsize = [6,5])
        f(ax, id, opt = opt, path = path)
        fig.savefig(output_path)
        plt.close('all')

    def plot_cv(self, fname, f, opt = None, path = '') -> None:
        output_path = path + 'plot/' + fname + '.png'
        print('Creating ' + output_path + ' ...')
        fig, axs = plt.subplots(3, 4, figsize = [30,17])
        for i in range(4):
            for j in range(3):
                f(axs[j,i], 'VP' + str(i) + '-' + str(j), opt = opt, path = path)
        fig.savefig(output_path)
        plt.close('all')


