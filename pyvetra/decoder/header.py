import numpy as np

class Header():
    def __init__(
        self,
        byte_header : bytes = None
    ):
        """
        """
        if byte_header != None:
            self.assemble(byte_header)

    def assemble(self, hb):
        self.vp = 'Module' + str(hb[0]).zfill(2) + '_VP' + str(hb[1]) + '-' + str(hb[2])
        self.module = hb[0]
        self.trim = hb[11]
        self.time = int.from_bytes(hb[3:11], 'little')
        self.min_thr = int.from_bytes(hb[12:16], 'little')
        self.max_thr = int.from_bytes(hb[16:20], 'little')
        self.step_size = int.from_bytes(hb[20:24], 'little')
        self.n_of_steps = int(np.ceil((float(self.max_thr) - float(self.min_thr))/int(self.step_size)))
        self.shutter_time = int.from_bytes(hb[24:28], 'little')
        self.readable = self.vp + ' ' + str(self.time) + ' ' + str(self.trim)
        self.readable += ' ' + str(self.min_thr) + ' ' + str(self.max_thr) + ' '
        self.readable += str(self.step_size) + ' ' + str(self.shutter_time)

    def fill(self, hdl):
        try:
            self.read_to_bin(hdl)
        except:
            try:
                self.read_to_bin_old(hdl)
            except:
                pass
        return None

    def read_to_bin(self, readable):
        head = readable.split(' ')
        self.vp = head[1]
        self.time = int(head[2])
        self.trim = int(head[3])
        self.min_thr = int(head[4])
        self.max_thr = int(head[5])
        self.step_size = int(head[6])
        self.n_of_steps = int((int(self.max_thr) - int(self.min_thr))/int(self.step_size))
        self.shutter_time = int(head[7][:-1])

    def read_to_bin_old(self, readable):
        head = readable.split(' ')
        self.vp = head[0][1:]
        self.time = int(head[5][1:][:-1])
        #... to be filled (if needed at some point)

#Mod25VP00 ASIC300 trim0 type1 2023-06-27_11-49-59 (1687859399) pkopciew elog1 runs0 120 5 1310720000 1100 1700 0 0 0 0 0
    
    def is_valid(self, fsize):
        if fsize == (self.n_of_steps * 256 * 192 + 28):
            return True
        else:
            return False

    def get_boundary(self):
        return int(self.header[12]), int(self.header[13])
    
