import numpy as np
import os
import matplotlib.pyplot as plt
import random
import glob
import datetime
import linecache
import pandas as pd
import matplotlib.ticker as ticker
import logging

from utils import nametmp as nt
from utils import asicinfo, functions
from analyse import analyse
from plot import clb
from decoder import header
from plot import cnv_universal as cnvu
from matplotlib.colors import LinearSegmentedColormap
from decoder import recipe_handler
from plot import plot
from cycler import cycler

import json

class Status(recipe_handler.Recipe_handler, plot.Plot):
    def __init__(
        self
    ):
        '''
        '''


    # if_scan verifies if .dat file exists and has an expected size in bits
    def if_scan(self, mod, vp, trim, path):
        if_exist = 0
        fp = path #+ '/Module' + str(mod).zfill(2) + '/' + tp
        fp += '/Module' + str(mod).zfill(2) + '_' + vp + '_scan' + trim + '_1of1.dat'
        try:
            f = open(fp, 'rb')
            head = header.Header(f.read(28))
            if head.is_valid(os.path.getsize(fp)):
                return True
            else:
                return False
        except:
            return False

    def make_overview(self, dirc, which):
        #create a master file
        df = functions.build_velo_df(['if_exist'])

        vg = self.map_if_exist(dirc, which = which)
        df['if_exist'][:] = vg.ravel().astype(int)
        if which == 'normal' or which == 'quick':
            df['underflow_0'] = self.detect_overflow(dirc, which = which, side = 'left', trim = '0').ravel(order = 'F')
            df['overflow_0'] = self.detect_overflow(dirc, which = which, side = 'right', trim = '0').ravel(order = 'F')
            df['underflow_15'] = self.detect_overflow(dirc, which = which, side = 'left', trim = '15').ravel(order = 'F')
            df['overflow_15'] = self.detect_overflow(dirc, which = which, side = 'right', trim = '15').ravel(order = 'F')
            df['mean_15'] = self.find_mean(dirc, which = which, trim = '15').ravel(order = 'F')
            df['std_15'] = self.find_std(dirc, which = which, trim = '15').ravel(order = 'F')
        elif which == 'control':
            df['underflow_16'] = self.detect_overflow(dirc, which = which, side = 'left', trim = '16').ravel(order = 'F')
            df['overflow_16'] = self.detect_overflow(dirc, which = which, side = 'right', trim = '16').ravel(order = 'F')
            #df['baseline_mean'], df['baseline_std'] = self.find_baseline_prop(
            #                                            dirc, which = which,trim = '16').ravel(order = 'F')
            #df['baseline_std'] = self.find_baseline_std(dirc, which = which, trim = '16').ravel(order = 'F')
            #df['rate_mean'], df['rate_std'] = self.find_rate_mean(
            #                                    dirc, which = which, trim = '16').ravel(order = 'F')
            #df['masked_at_1000'] = self.find_masked(dirc).ravel(order = 'F')
            


        df.to_csv('studies/' + which + '_status.csv')

        return None
        #for mod in range(52):
        #    path = dirc + '/Module' + str(mod).zfill(2) + '/quick/'
            
        #    #if os.path.exists(path):
        #    #
        #    #    for vp in asicinfo.asic_list:

    def status_manager(self, dirc):
        if not os.path.exists('studies'):
            os.makedirs('studies')
        self.plot_eq_status_summary(dirc)

        #self.plot_if_exist(dirc, 'quick')
        #self.plot_if_exist(dirc, 'normal')
        #self.plot_if_exist(dirc, 'control')
        #self.plot_if_exist_all(dirc)
        return None

    def find_mean(self, dirc, which, trim):
        arr = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2)
            if os.path.exists(path):
                for vp in asicinfo.asic_list:
                    vp_no = int(vp[2])*3 + int(vp[4])
                    scan, head = self.open_csv(vp, nt.name['n_rate_t' + trim], skiprows = 1, return_header = True, 
                    ravel = True, path = dirc + '/' + path + '/' + which + '/')
                    if head is None:
                        continue
                    n_mean = np.mean(scan, where = scan != 0)
                    #if which == 'normal':
                    #    print(n_sus, bd, scan, abs(scan-bd), head)
                    arr[vp_no, mod] = n_mean
        return arr

    def find_std(self, dirc, which, trim):
        arr = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2)
            if os.path.exists(path):
                for vp in asicinfo.asic_list:
                    vp_no = int(vp[2])*3 + int(vp[4])
                    scan, head = self.open_csv(vp, nt.name['n_rate_t' + trim], skiprows = 1, return_header = True, 
                    ravel = True, path = dirc + '/' + path + '/' + which + '/')
                    if head is None:
                        continue
                    n_std = np.std(scan, where = scan != 0)
                    #if which == 'normal':
                    #    print(n_sus, bd, scan, abs(scan-bd), head)
                    arr[vp_no, mod] = n_std
        return arr

    def detect_overflow(self, dirc, which, side, trim):
        arr = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2)
            if os.path.exists(path):
                for vp in asicinfo.asic_list:
                    vp_no = int(vp[2])*3 + int(vp[4])
                    scan, head = self.open_csv(vp, nt.name['n_rate_t' + trim], skiprows = 1, return_header = True, 
                    ravel = True, path = dirc + '/' + path + '/' + which + '/')
                    if head is None:
                        continue
                    hd = header.Header()
                    hd.read_to_bin(head)
                    if side == 'left':
                        bd = hd.min_thr
                    elif side == 'right':
                        bd = hd.max_thr
                    n_sus = np.count_nonzero(abs(scan - bd) < 10)
                    #if which == 'normal':
                    #    print(n_sus, bd, scan, abs(scan-bd), head)
                    arr[vp_no, mod] = n_sus
        return arr

    def status_if_actual(self, dirc):
        output_path = 'studies/if_actual_over.pdf'
        print('... creating ' + output_path)
        fig, ax = cnvu.velo_map(figsize = [14,8])
        if_actual = np.zeros([12,52])
        for mod in range(52):
            pathc = 'Module' + str(mod).zfill(2) + '/control/'
            pathn = 'Module' + str(mod).zfill(2) + '/normal/'
            if os.path.exists(pathc) and os.path.exists(pathn):
                for vp in asicinfo.asic_list:
                    try:
                        c_dt = os.path.getmtime(pathc + '/Module' + str(mod).zfill(2) +
                                 '_' + vp + '_scan16_1of1.dat' )
                        n0_dt = os.path.getmtime(pathn + '/Module' + str(mod).zfill(2) +
                                 '_' + vp + '_scan0_1of1.dat' )
                        n15_dt = os.path.getmtime(pathn + '/Module' + str(mod).zfill(2) +
                                 '_' + vp + '_scan15_1of1.dat' )

                        id_no = int(vp[2])*3 + int(vp[4])
                        #print(n0_dt, n15_dt, c_dt)
                        if n15_dt < n0_dt:
                            if_actual[id_no, mod] = 0
                        elif c_dt < n15_dt:
                            if_actual[id_no, mod] = 1
                        else:
                            if_actual[id_no, mod] = 2
                        #else:
                        #    n_spread[id_no, mod] = 1000
                    except Exception as e: print(e)
        
        #fig.savefig(output_path)

        abstract_plot = ax.imshow(if_actual, interpolation = 'none', origin = 'lower', cmap = 'jet')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title('Masked pixels')
        abstract_plot.set_clim(0,2)
        ax.set_title('Masked pixels for 50 DAC threshold')
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)


        if_actual = if_actual.astype(int)
        #print(n_spread)
        for (j, i), label in np.ndenumerate(if_actual):
            #if label != 0 and label < 1000:
            ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 5, color = 'k')
        #nothing
        fig.savefig(output_path)
        return None
    
    def make_calib_summary(self, dirc):
        fsum = open('calib_summary.js', 'w')
        fsum.write('export const calib_summary = [\n')

        n_masked = np.zeros([12,52], dtype = int)
        n_thr = np.zeros([12,52], dtype = int)
        n_sigma = np.zeros([12,52])
        n_split = np.zeros([12,52])
        n_vfbk = np.zeros([12,52], dtype = int)
        for mod in range(52):
            print('Module', mod)
            path = 'Module' + str(mod).zfill(2) + '/control/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        id_no = int(id[2])*3 + int(id[4])
                        rate = self.open_csv(id, nt.name['n_rate_t16'], skiprows = 1, path = path)
                        base = self.open_csv(id, nt.name['n_mean_t16'], skiprows = 1, path = path)
                        sigma = self.open_csv(id, nt.name['n_width_t16'], skiprows = 1, path = path)
                        mbase = np.mean(base, where = base!= 0)
                        msigma = np.mean(sigma, where = sigma!= 0)
                        msigma_odd = np.mean(sigma[:,1::2], where = sigma[:,1::2]!= 0)
                        msigma_even = np.mean(sigma[:,::2], where = sigma[:,::2]!= 0)
                        print(msigma_odd, msigma_even)
                        n_split[id_no, mod] = msigma_odd - msigma_even
                        masked = np.count_nonzero((mbase + 70 - rate) < 0)
                        #id_no = int(id[2])*3 + int(id[4])
                        n_masked[id_no, mod] = masked
                        n_sigma[id_no, mod] = msigma
                        thr = mbase + 60
                        n_thr[id_no, mod] = int(thr)
                    except:
                        pass
        #n_masked1D = n_masked.ravel(order = 'F')


        for mod in range(52):
            for vp in range(12): #for vp in asicinfo.asic_list:
                fsum.write('{')
                fsum.write('Asic: "' + str(mod*12 + vp) + '",') #"Module' + str(mod).zfill(2) + vp
                fsum.write('Masked_pixels: "' + str(n_masked[vp, mod]) + '",') #"Module' + str(mod).zfill(2) + vp
                fsum.write('Global_threshold: "' + str(n_thr[vp, mod]) + '",') #"Module' + str(mod).zfill(2) + vp
                fsum.write('Noise_sigma: "' + f'{n_sigma[vp, mod]:.2f}' + '",')
                fsum.write('Noise_split: "' + f'{n_split[vp, mod]:.2f}' + '",')
                #fsum.write('VFBK: "' + str(n_vfbk[vp, mod]) + '",')
                fsum.write("},\n")
        fsum.write(']')
        fsum.close()


    def status_mean(self, dirc):
        output_path = 'studies/rate_over.pdf'
        print('... creating ' + output_path)
        fig, ax = cnvu.velo_map(figsize = [14,8])
        df = pd.read_csv('studies/normal_status.csv')
        narr = df['if_exist'].to_numpy().reshape([52,12]).T
        narr = narr.astype(int)
        n_spread = np.zeros([12,52])
        n_mean = df['mean_15'].to_numpy().reshape([52,12]).T
        
        #fig.savefig(output_path)

        abstract_plot = ax.imshow(n_mean, interpolation = 'none', origin = 'lower', cmap = 'jet')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title('Threshold [DAC]')
        abstract_plot.set_clim(1500,2000)
        ax.set_title('Noise baseline from trim F threshold scan')
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)


        n_mean = n_mean.astype(int)
        #print(n_spread)
        for (j, i), label in np.ndenumerate(n_mean):
            #if label != 0 and label < 1000:
            ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 4, color = 'k')
        #nothing
        fig.savefig(output_path)
        return None


    def status_spread(self, dirc):
        output_path = 'studies/pixels_over_120dac.pdf'
        print('... creating ' + output_path)
        fig, ax = cnvu.velo_map(figsize = [14,8])
        #df = pd.read_csv('studies/control_status.csv')
        #narr = df['if_exist'].to_numpy().reshape([52,12]).T
        #narr = narr.astype(int)
        n_masked = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/control/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        #rate = self.open_csv(id, nt.name['n_rate_t16'], skiprows = 1, path = path)
                        base = self.open_csv(id, nt.name['n_mean_t16'], skiprows = 1, path = path)

                        mbase = np.mean(base, where = base!= 0)
                        pixs = np.count_nonzero((mbase - 60 - base) > 0)
                        #thr = mbase + 60
                        #print(mbase)
                        #masked = np.count_nonzero((mbase + 60 - rate) < 0)
                        #print(np.count_nonzero(rate == 0))
                        #print(mbase + 70, rate)
                        #mask = self.build_mask_pattern()
                        #odd_mean = np.mean(data[mask == 1])
                        #even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        #if narr[id_no, mod] == 1:
                        n_masked[id_no, mod] = pixs
                        #else:
                        #    n_spread[id_no, mod] = 1000
                    except:
                        pass

        #json build
        n_masked1D = n_masked.ravel(order = 'F')
        velodict = functions.velomap2dict(n_masked1D, toint = True)
        with open("baseline_spread.json", "w") as fp:
            json.dump(velodict, fp)

        #fig.savefig(output_path)
        avr = np.mean(np.mean(n_masked, where = n_masked > 1))
        abstract_plot = ax.imshow(n_masked, interpolation = 'none', origin = 'lower', cmap = 'YlOrRd')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title('Pixels')
        abstract_plot.set_clim(0,500)
        ax.set_title('Pixels of eff. thr. > 120 DAC (1720 e), global thr. 70 DAC (1000 e)')
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)


        n_masked = n_masked.astype(int)
        ax.text(0,12.3,'Avg. no. of pixels: ' + str(int(avr)), fontsize = 9.5)
        #print(n_spread)
        for (j, i), label in np.ndenumerate(n_masked):
            if label > 1 :
                ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 4, color = 'k')
        #nothing
        fig.savefig(output_path)
        return None


    def status_threshold(self, dirc):
        output_path = 'studies/threshold_over_70dac.pdf'
        print('... creating ' + output_path)
        fig, ax = cnvu.velo_map(figsize = [14,8])
        #df = pd.read_csv('studies/control_status.csv')
        #narr = df['if_exist'].to_numpy().reshape([52,12]).T
        #narr = narr.astype(int)
        n_masked = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/control/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        #rate = self.open_csv(id, nt.name['n_rate_t16'], skiprows = 1, path = path)
                        base = self.open_csv(id, nt.name['n_mean_t16'], skiprows = 1, path = path)

                        mbase = np.mean(base, where = base!= 0)
                        thr = mbase + 60
                        #print(mbase)
                        #masked = np.count_nonzero((mbase + 60 - rate) < 0)
                        #print(np.count_nonzero(rate == 0))
                        #print(mbase + 70, rate)
                        #mask = self.build_mask_pattern()
                        #odd_mean = np.mean(data[mask == 1])
                        #even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        #if narr[id_no, mod] == 1:
                        n_masked[id_no, mod] = thr
                        #else:
                        #    n_spread[id_no, mod] = 1000
                    except:
                        pass

        #json build
        n_masked1D = n_masked.ravel(order = 'F')
        n_masked1D[np.isnan(n_masked1D)] = 0
        velodict = functions.velomap2dict(n_masked1D, toint = True)
        with open("global_threshold.json", "w") as fp:
            json.dump(velodict, fp)

        #fig.savefig(output_path)
        avr = np.mean(np.mean(n_masked, where = n_masked > 1))
        abstract_plot = ax.imshow(n_masked, interpolation = 'none', origin = 'lower', cmap = 'GnBu')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title('Threshold')
        abstract_plot.set_clim(1500,1900)
        ax.set_title('Threshold in DAC code fixed at 70 DAC (1000 e)')
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)


        n_masked = n_masked.astype(int)
        ax.text(0,12.3,'Avg. threshold: ' + str(int(avr)), fontsize = 9.5)
        #print(n_spread)
        for (j, i), label in np.ndenumerate(n_masked):
            if label > 1 :
                ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 4, color = 'k')
        #nothing
        fig.savefig(output_path)
        return None


    def status_masked(self, dirc):
        output_path = 'studies/masked_over_70dac.png'
        print('... creating ' + output_path)
        fig, ax = cnvu.velo_map(figsize = [28,16])
        #df = pd.read_csv('studies/control_status.csv')
        #narr = df['if_exist'].to_numpy().reshape([52,12]).T
        #narr = narr.astype(int)
        n_masked = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/control/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        rate = self.open_csv(id, nt.name['n_rate_t16'], skiprows = 1, path = path)
                        base = self.open_csv(id, nt.name['n_mean_t16'], skiprows = 1, path = path)
                        
                        mbase = np.mean(base, where = base!= 0)
                        #print(mbase)
                        masked = np.count_nonzero((mbase + 70 - rate) < 0)
                        #print(np.count_nonzero(rate == 0))
                        #print(mbase + 70, rate)
                        #mask = self.build_mask_pattern()
                        #odd_mean = np.mean(data[mask == 1])
                        #even_mean = np.mean(data[mask == 2])
                        id_no = int(id[2])*3 + int(id[4])
                        #if narr[id_no, mod] == 1:
                        n_masked[id_no, mod] = masked
                        #else:
                        #    n_spread[id_no, mod] = 1000
                    except:
                        pass

        #json build
        n_masked1D = n_masked.ravel(order = 'F')
        velodict = functions.velomap2dict(n_masked1D, toint = True)
        with open("masked_pixels.json", "w") as fp:
            json.dump(velodict, fp)

        #fig.savefig(output_path)
        avr = np.mean(np.mean(n_masked, where = n_masked < 250))
        abstract_plot = ax.imshow(n_masked, interpolation = 'none', origin = 'lower', cmap = 'Wistia')
        clba = fig.colorbar(abstract_plot, shrink = 0.4, orientation = 'horizontal')
        clba.ax.set_title('Masked pixels', fontsize = 16)
        clba.ax.tick_params(labelsize = 15)
        abstract_plot.set_clim(0,400)
        ax.set_title('Masked pixels for 70 DAC (1000 e) threshold', fontsize = 20)
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)


        n_masked = n_masked.astype(int)
        ax.text(0,12.3,'Avg. masked (<250): ' + str(int(avr)), fontsize = 18.5) 
        #print(n_spread)
        for (j, i), label in np.ndenumerate(n_masked):
            if label != 0 and label < 999:
                ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 14, color = 'k')
        #nothing
        fig.savefig(output_path)
        return None

    def status_dacsettings(self, path_recipe, recipe_tag=''):
        df = self.recipe_to_df(path_recipe, recipe_tag)
     
        for dac_type in asicinfo.dac_settings: 
            output_path ='dacval_'+ dac_type +f'_over_{recipe_tag}.pdf' 
            print('... creating ' + output_path)
            fig, ax = cnvu.velo_map(figsize = [14,8])  
            narr = df[dac_type].to_numpy().reshape([52,12]).T
            #narr[np.isnan(narr)]=0 #put the nan to 0
            narr = narr.astype(int)

            #modifying jet colormap,
            jet_cmap = plt.get_cmap('jet')
            new_colors = jet_cmap(np.linspace(0,1,256))
            new_colors[0] = [1,1,1,1] # se the first color to white

            #create custom map
            custom_map = LinearSegmentedColormap.from_list('custom_jet', new_colors, N=256)

            abstract_plot = ax.imshow(narr, interpolation = 'none', origin = 'lower', cmap = custom_map, vmin=40)
            clba = fig.colorbar(abstract_plot, shrink = 0.45)
            clba.ax.set_title(dac_type)
            abstract_plot.set_clim(30,255)
            ax.set_title(dac_type)
            ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
            ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)

            for (j, i), label in np.ndenumerate(narr):
                if label != 0 and label < 1000 and label> 30 and label != np.nan and label != np.isnan(label):
                    ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 5, color = 'k')   
            fig.savefig(output_path)

        return None

    def correlation_dacsettings_noise(self, path_recipe, recipe_tag=''):

        # Defining cycler
        #colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
        markers = ['o', 's', '^', 'D', 'v', 'p', 'h']
        #color_cycler = cycler('color', colors)
        marker_cycler = cycler('marker', markers)
        # Combine the color and marker cyclers
        cycle =  marker_cycler
        # Apply the cycle to the entire plot
        plt.rc('axes', prop_cycle=cycle)

        recipe_df = self.recipe_to_df(path_recipe, recipe_tag)

        csv_present = any(filename.endswith('df.csv') for filename in os.listdir())
        if csv_present:
            print('Reading existing scan csv file...')
            scan_df = pd.read_csv(f'scan_df.csv')
        elif (not csv_present):
            print('Creating scan csv file...')
            scan_df = self.noise_properties_df()           
        for noise_property in asicinfo.noise_properties:
            try:               
                plt.figure()
                for dac_type in asicinfo.dac_settings:
                    if dac_type == 'IKRUM':
                        continue
                    x = recipe_df[dac_type].to_numpy() 
                    x = np.nan_to_num(x, nan=0) 
                    y = scan_df[noise_property].to_numpy()
                    y = np.nan_to_num(y, nan=0) 
                    R_matrix = np.corrcoef([ x, y ])

                    plt.scatter(recipe_df[dac_type], scan_df[noise_property], s=0.5, label = f'{dac_type}, R={round(R_matrix[0,1],2)}')
                    plt.legend(fontsize=7, loc = 'upper left')
                    plt.xlabel('DAC')
                    plt.ylabel(noise_property)
                    plt.savefig(f'plots/prova_{noise_property}.pdf')
            except:
                print('Check that the existing csv file is complete, if not please remove it and it will be recreated.')
        return None
        


    def plot_eq_status_summary(self, dirc):
        output_path = 'studies/if_exist_overview.pdf'
        print('Creating ' + output_path + ' ...')
        fig, ax = cnvu.velo_map(figsize = [14,8])
        df = pd.read_csv('studies/quick_status.csv')
        narr = df['if_exist'].to_numpy().reshape([12,52])
        df = pd.read_csv('studies/normal_status.csv')
        overflow = df[['underflow_0', 'overflow_0', 'underflow_15', 'overflow_15']].to_numpy() 
        narr = np.add(narr, df['if_exist'].to_numpy().reshape([12,52]), out = narr, where = narr == 1.)
        print(narr)
        df = pd.read_csv('studies/control_status.csv')
        overflow2 = df[['overflow_16']].to_numpy()
        narr = np.add(narr, df['if_exist'].to_numpy().reshape([12,52]), out = narr, where = narr == 2.)
        
        #self.check_validity()

        #print(narr)
        narr = narr.astype(int)
        abstract_plot = ax.imshow(narr, interpolation = 'none', origin = 'lower', cmap = clb.eq_overview_all())
        bounds = np.linspace(0, 3, 5)
        clba = fig.colorbar(abstract_plot, ticks = bounds, boundaries = bounds, shrink = 0.45)
        clba.ax.set_title("Status", fontsize = 10)

        def label_cbrt(x,pos):
            if x == 0:
                x = '-'
            if x == 1:
                x = 'Q (quick scan)'
            if x == 2:
                x = 'N (normal scan)'
            if x == 3:
                x = 'C (control scan)'
            return x

        clba.set_ticks(np.linspace(0,3,4))
        clba.formatter = ticker.FuncFormatter(label_cbrt)
        clba.update_ticks()

        abstract_plot.set_clim(0,3)
        ax.hlines(y=np.arange(0, 12)+0.5, xmin=np.full(12, 0)-0.5, xmax=np.full(12, 52)-0.5, color="black", lw = 0.5)
        ax.vlines(x=np.arange(0, 52)+0.5, ymin=np.full(52, 0)-0.5, ymax=np.full(52, 12)-0.5, color="black", lw = 0.5)
        ax.set_title('Equalisation overview status: Summer23', fontsize = 13)

        for (j, i), label in np.ndenumerate(narr):
            #print(j, i)
            suffix = ''
            if any(overflow[j + i*12] > 150) or any(overflow2[j + i*12]> 150):
                suffix = '(!)' 
            if label == 0:
                label = '-'
            if label == 1:
                label = 'Q' + suffix
            if label == 2:
                label = 'N' + suffix
            if label == 3:
                label = 'C' + suffix
            if suffix == '(!)':
                ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 6, color = 'r', fontweight = 'demibold')
            else:
                ax.text(i,j,label, ha = 'center', va = 'center', fontsize = 6)       
        #def format_coord(x,y):
        #    return "text_string_made_from({:.2f},{:.2f})".format(x,y)
        #ax.format_coord=format_coord
        fig.savefig(output_path)
    '''
    def plot_if_exist(self, dirc, which):
        output_path = 'studies/' + which + '_overview.png'
        print('Creating ' + output_path + ' ...')
        fig, ax = cnvu.velo_map(figsize = [14,8])
        vg = np.zeros([12,52])
        vg = self.map_if_exist(dirc, which = which, arr = vg, req = 0)
        abstract_plot = ax.imshow(vg, interpolation = 'none', origin = 'lower', cmap = 'Greens')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Pixel av. baseline [DAC]", fontsize = 10)
        abstract_plot.set_clim(0,3)
        def format_coord(x,y):
            return "text_string_made_from({:.2f},{:.2f})".format(x,y)
        ax.format_coord=format_coord
        fig.savefig(output_path)
    '''
    def map_if_exist(self, dirc, which):
        arr = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2)
            if os.path.exists(path):
                for vp in asicinfo.asic_list:
                    vp_no = int(vp[2])*3 + int(vp[4])
                    if which == 'quick' or which == 'normal':
                        if (self.if_scan(mod, vp, '0' , dirc + '/' + path + '/' + which + '/') and
                            self.if_scan(mod, vp, '15', dirc + '/' + path + '/' + which + '/')):
                            arr[vp_no, mod] += 1
                    elif which == 'control':
                        if self.if_scan(mod, vp, '16' , dirc + '/' + path + '/' + which + '/'):
                            arr[vp_no, mod] += 1
        return arr



    #def map_if_exist
        '''
        if not os.path.exists('studies'):
            os.makedirs('studies')

        output_path = 'studies/velo_baseline_map.png'
        print('Creating ' + output_path + ' ...')
        fig, ax = cnvu.velo_map(figsize = [14,8])

        vg = np.zeros([12,52])
        for mod in range(52):
            path = 'Module' + str(mod).zfill(2) + '/quick/'
            if os.path.exists(path):
                for id in asicinfo.asic_list:
                    try:
                        data = self.open_csv(id, nt.name['n_mean_t0'], skiprows = 1, path = path)
                        id_no = int(id[2])*3 + int(id[4])
                        vg[id_no, mod] = np.mean(data, where = data != 0)
                    except:
                        pass
        abstract_plot = ax.imshow(vg, interpolation = 'none', origin = 'lower', cmap = 'Reds')
        clba = fig.colorbar(abstract_plot, shrink = 0.45)
        clba.ax.set_title("Pixel av. baseline [DAC]", fontsize = 10)
        abstract_plot.set_clim(1200,1900)
        def format_coord(x,y):
            return "text_string_made_from({:.2f},{:.2f})".format(x,y)
        ax.format_coord=format_coord
        plt.show()
        fig.savefig(output_path)
        '''
